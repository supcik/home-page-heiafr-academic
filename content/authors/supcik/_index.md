---
# Display name
name: Jacques Supcik

# Username (this should match the folder name)
authors:
- supcik

# Is this the primary user of the site?
superuser: true

# Role/position
role: Professor of Computer Science and Communication Systems

# Organizations/Affiliations
organizations:
- name: Haute école d'ingénierie et d'architecture de Fribourg
  url: "https://www.heia-fr.ch/en/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Programming languages
- Algorithmes and Data structures
- Embedded Systems and IoT
- Linux Operating Systems
- Cloud Computing
- Machine Learning

education:
  courses:
  - course: Doctor of Technical Science
    institution: ETH Zürich
    year: 1999
  - course: Dipl. Informatik-Ing.
    institution: ETH Zürich
    year: 1992
  - course: "Maturité Type C (scientific)"
    institution: Collège St-Michel, Fribourg
    year: 1987

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:jacques.supcik@hefr.ch'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/supcik
- icon: github
  icon_pack: fab
  link: https://github.com/supcik
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/supcik
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/supcik/

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "jacques.supcik@hefr.ch"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- ISC
- iSIS
---

Jacques Supcik is a full time professor in the "[Computer Science Communication Systems](https://www.heia-fr.ch/en/education/bachelor/computer-science-and-communication-systems/)" faculty of the the "[Haute école d'ingénierie et d'architecture](https://www.heia-fr.ch/)" of Fribourg.
In his free time, he continue to write code, learn things and tinker with technology, but once a week, he go out with his horse for a long [ride](https://www.facebook.com/centre.equestre.corminboeuf/) in the forrest.

His favorite programming languages are [Go](https://golang.org/) and [Python](https://www.python.org/). He also like to code and Kotlin and he plan to learn Rust soon. He teaches Java for the students in the first year (Introduction to programming) and C and ARM assembler for the students in the second year (Embedded systems). He also gives a lecture on Operating Systems.