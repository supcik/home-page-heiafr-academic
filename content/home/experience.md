+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Professor"
  company = "Haute école d'ingénierie et d'architecture"
  company_url = "https://www.heia-fr.ch/"
  location = "Fribourg"
  date_start = "2011-09-01"
  date_end = ""
  description = """
* Java programming and Algorithms and Data structures lectures for the first-year students (6 periods per week).
* Operating Systems and Concurrent Programming lectures for the second year students (4 periods per week).
*  Embedded Systems lecture for the second year students (4 periods per week).
* Member of the Applied Research Institute for Smart and Secure Systems.
"""

[[experience]]
  title = "Team Leader"
  company = "Swisscom (Suisse) SA"
  company_url = "https://www.swisscom.ch/"
  location = "Zürich/Bern"
  date_start = "2007-01-01"
  date_end = "2013-09-30"
  description = """
* Head of the “Security Solution Sysadmin” team (sites of Bern and Zürich).
* Operation of the Swisscom PKI, recognized according to the Swiss Certification Law (SCSE/ZertES).
* Operation of 200+ UNIX and GNU/Linux servers across multiple sites, with 99.999% availability for the most critical
services.
* Optimization and improvement of service quality through the establishment of the “DevOps” culture in the team.
"""

[[experience]]
  title = "Team leader and senior developer"
  company = "Swisscom (Suisse) SA"
  company_url = "https://www.swisscom.ch/"
  location = "Zürich/Bern"
  date_start = "1999-06-01"
  date_end = "2007-01-31"
  description = """
* Development of the “Swisscom Digital Certificate Service” (Public-Key Infrastructure).
* Design and implementation of web portals (Internet and Extranet).
* Design of the end-to-end internal monitoring system using Nagios.
* Management and development of several projects to improve IP-Plus Internet services.
"""

# [[experience]]
#   title = "System administrator"
#   company = "ETH Zürich"
#   company_url = "https://ethz.ch/"
#   location = "Zürich"
#   date_start = "1998-10-01"
#   date_end = "1999-06-30"
#   description = """
# * Management of Sun servers and workstations.
# """

# [[experience]]
#   title = "Software developer (Modula-2)"
#   company = "Hiware AG (acquired by Motorola in 2000)"
#   company_url = ""
#   location = "Zürich"
#   date_start = "1992-06-01"
#   date_end = "1993-10-31"
#   description = """
# * Efficient implementation of floating point divisions for Motorola microprocessors. My implementation was more
# compact and faster than in any other competitor on the market.
# """


+++
