---
# Course title, summary, and position.
linktitle: Systèmes embarqués 2
summary: Comprendre l'architecture des ordinateurs et programmer des systèmes embarqués en C et en assembleur.
weight: 1
# Page metadata.
title: Systèmes embarqués 2
date: "2020-01-28"
lastmod: "2020-01-28"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  embedded-systems-2:
    name: SE-2
    weight: 1
---

## Objectifs

De façon globale le cours de systèmes embarqués 2 vise les objectifs suivants:

- Décrire et expliquer les architectures générales des microprocesseurs
- Concevoir et programmer des traitements interruptifs en langages assembleur et C
- Interfacer au niveau logiciel un microprocesseur avec différents périphériques
- Décrire et expliquer les notions de DMA, mémoire cache, mémoire virtuelle (MMU), pipelining

De façon plus détaillée, et au terme du cours, le participant sera capable :

- de décrire l'architecture générale des microprocesseurs
- de décrire l'architecture interne des microprocesseurs
- de traiter les interruptions et les exceptions
- d'interfacer un périphérique à un microprocesseur en mode interruptif et d'en programmer les fonctions de base
- de programmer les éléments de base du noyau d'un système d'exploitation
- de concevoir et programmer un pilote logiciel simple
- d'interfacer et/ou de configurer un mécanisme d'accès de mémoire direct (DMA) entre un périphérique et un microprocesseur
- de décrire et de configurer le contrôleur de la mémoire cache et de l'unité de gestion de la mémoire (MMU)

## Contenu

Le cours traitera des sujets suivants:

- Architecture générale des microprocesseurs (Von Neuman, Harvard, SIMD, MIMD)
- Architecture interne des microprocesseurs
- Programmation interruptive
- Interconnection de périphériques usuels
- Eléments du noyau d'un système d'exploitation (thread, sémaphore, etc.)
- Direct Access Memory (DMA)
- Mémoires caches
- Memory Management Unit (MMU)

## Spécification du cours

| Forme d'enseignement                  | volume de travail |
|---------------------------------------|----------------   |
| Cours magistral (y compris exercices) | 32 périodes       |
| Travaux pratiques / laboratoires      | 32 périodes       |
| Examen de révision                    | oral (15 min.)    |
   

Poids : 4\
Année de validité : 2019-2020\
Année du plan d'études : 2ème année\
Semestre : Printemps\
Programme : Français,Bilingue\
Filière : Informatique\
Langue d'enseignement : Français\
Identifiant : B2C-SEM2-I\
Niveau : Intermédiaire\
Type de cours : Fondamental\
Formation : Bachelor\
Modalités d'évaluation : Contrôle continu: travaux écrits, TP/évaluation de rapports\
Examen : oral (15 min.)

## Mode de calcul de la note de cours

$$\text{Note cours} = \text{Note théorique} + \frac{\text{Note pratique} - 5}{3}$$

$$\text{Note Finale} = \frac{\text{Note cours} + \text{Examen}}{2}$$

## Ouvrages de référence

- ARM System Developer's Guide - Designing and Optimizing System Software, Andrew N. Sloss, Dominic Symes, Chris Wright, Morgan Kaufmann Publishers, 2004
- ARM Assembly Language - Fundamentals and Techniques, William Hohl, CRC Press, 2009
- Computer Systems - A Programmer's Perspective, Randal E. Bryant, David R. O'Hallaron, Prentice Hall, 2011
- Inside the Machine - An Illustrated Introduction to Microprocessors and Computer Architecture, Jon Stokes, ars technica library, 2007
- ARM System-on-Chip, Steve Furber, Addison-Wesley, 2000
- ARM Assembly Language - an introduction, J.R. Gibson, University of Liverpool, 2007
- The C Programming Language, Brian W. Kernighan, Dennis M. Ritchie, Prentice Hall, 1988
- Programming in C - A complete introduction to the C programming language, Stephen G. Kochan, Developer's Library, 2005

## Enseignant(s) et/ou coordinateur(s)

Daniel Gachet, Jacques Supcik
