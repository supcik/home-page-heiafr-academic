---
title: Liens utiles pour le cours
linktitle: Liens utiles
toc: true
type: docs
date: "2019-05-05T00:00:00+01:00"
draft: false
menu:
  embedded-systems-2:
    #parent: Liens
    weight: 2

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 1
---

- [Dépot git pour le cours](https://gitlab.forge.hefr.ch/se12-1920/lecture)
- [Dépot git pour les TPs](https://gitlab.forge.hefr.ch/se12-1920/tp)
